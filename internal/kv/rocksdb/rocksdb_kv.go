// Copyright (C) 2019-2020 Zilliz. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
// with the License. You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software distributed under the License
// is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
// or implied. See the License for the specific language governing permissions and limitations under the License.

package rocksdbkv

import (
	"errors"
	"fmt"

	"github.com/tecbot/gorocksdb"
)

type RocksdbKV struct {
<<<<<<< HEAD
	opts         *gorocksdb.Options
	db           *gorocksdb.DB
	writeOptions *gorocksdb.WriteOptions
	readOptions  *gorocksdb.ReadOptions
=======
	Opts         *gorocksdb.Options
	DB           *gorocksdb.DB
	WriteOptions *gorocksdb.WriteOptions
	ReadOptions  *gorocksdb.ReadOptions
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	name         string
}

func NewRocksdbKV(name string) (*RocksdbKV, error) {
	if name == "" {
		return nil, errors.New("rocksdb name is nil")
	}
	bbto := gorocksdb.NewDefaultBlockBasedTableOptions()
	bbto.SetBlockCache(gorocksdb.NewLRUCache(3 << 30))
	opts := gorocksdb.NewDefaultOptions()
	opts.SetBlockBasedTableFactory(bbto)
	opts.SetCreateIfMissing(true)

	ro := gorocksdb.NewDefaultReadOptions()
	ro.SetFillCache(false)

	wo := gorocksdb.NewDefaultWriteOptions()
	db, err := gorocksdb.OpenDb(opts, name)
	if err != nil {
		return nil, err
	}
	return &RocksdbKV{
<<<<<<< HEAD
		opts:         opts,
		db:           db,
		writeOptions: wo,
		readOptions:  ro,
=======
		Opts:         opts,
		DB:           db,
		WriteOptions: wo,
		ReadOptions:  ro,
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
		name:         name,
	}, nil
}

func (kv *RocksdbKV) Close() {
<<<<<<< HEAD
<<<<<<< HEAD
	kv.db.Close()
=======
	kv.DB.Close()
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
=======
	if kv.DB != nil {
		kv.DB.Close()
	}
>>>>>>> knowhere-2
}

func (kv *RocksdbKV) GetName() string {
	return kv.name
}

func (kv *RocksdbKV) Load(key string) (string, error) {
<<<<<<< HEAD
<<<<<<< HEAD
	value, err := kv.db.Get(kv.readOptions, []byte(key))
=======
=======
	if kv.DB == nil {
		return "", fmt.Errorf("Rocksdb instance is nil when load %s", key)
	}

>>>>>>> knowhere-2
	value, err := kv.DB.Get(kv.ReadOptions, []byte(key))
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	defer value.Free()
	return string(value.Data()), err
}

func (kv *RocksdbKV) LoadWithPrefix(key string) ([]string, []string, error) {
<<<<<<< HEAD
<<<<<<< HEAD
	kv.readOptions.SetPrefixSameAsStart(true)
	kv.db.Close()
	kv.opts.SetPrefixExtractor(gorocksdb.NewFixedPrefixTransform(len(key)))
	var err error
	kv.db, err = gorocksdb.OpenDb(kv.opts, kv.GetName())
=======
=======
	if key == "" {
		return nil, nil, errors.New("Key is nil in LoadWithPrefix")
	}
	if kv.DB == nil {
		return nil, nil, fmt.Errorf("Rocksdb instance is nil when load %s", key)
	}
>>>>>>> knowhere-2
	kv.ReadOptions.SetPrefixSameAsStart(true)
	kv.DB.Close()
	kv.Opts.SetPrefixExtractor(gorocksdb.NewFixedPrefixTransform(len(key)))
	var err error
	kv.DB, err = gorocksdb.OpenDb(kv.Opts, kv.GetName())
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	if err != nil {
		return nil, nil, err
	}

<<<<<<< HEAD
	iter := kv.db.NewIterator(kv.readOptions)
=======
	iter := kv.DB.NewIterator(kv.ReadOptions)
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	defer iter.Close()
	keys := make([]string, 0)
	values := make([]string, 0)
	iter.Seek([]byte(key))
	for ; iter.Valid(); iter.Next() {
		key := iter.Key()
		value := iter.Value()
		keys = append(keys, string(key.Data()))
		values = append(values, string(value.Data()))
		key.Free()
		value.Free()
	}
	if err := iter.Err(); err != nil {
		return nil, nil, err
	}
	return keys, values, nil
}

func (kv *RocksdbKV) ResetPrefixLength(len int) error {
	kv.DB.Close()
	kv.Opts.SetPrefixExtractor(gorocksdb.NewFixedPrefixTransform(len))
	var err error
	kv.DB, err = gorocksdb.OpenDb(kv.Opts, kv.GetName())
	return err
}

func (kv *RocksdbKV) MultiLoad(keys []string) ([]string, error) {
	if kv.DB == nil {
		return nil, errors.New("Rocksdb instance is nil when do MultiLoad")
	}
	values := make([]string, 0, len(keys))
	for _, key := range keys {
<<<<<<< HEAD
		value, err := kv.db.Get(kv.readOptions, []byte(key))
=======
		value, err := kv.DB.Get(kv.ReadOptions, []byte(key))
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
		if err != nil {
			return []string{}, err
		}
		values = append(values, string(value.Data()))
	}
	return values, nil
}

func (kv *RocksdbKV) Save(key, value string) error {
<<<<<<< HEAD
<<<<<<< HEAD
	err := kv.db.Put(kv.writeOptions, []byte(key), []byte(value))
=======
=======
	if kv.DB == nil {
		return errors.New("Rocksdb instance is nil when do save")
	}
>>>>>>> knowhere-2
	err := kv.DB.Put(kv.WriteOptions, []byte(key), []byte(value))
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	return err
}

func (kv *RocksdbKV) MultiSave(kvs map[string]string) error {
	if kv.DB == nil {
		return errors.New("Rocksdb instance is nil when do MultiSave")
	}
	writeBatch := gorocksdb.NewWriteBatch()
	defer writeBatch.Clear()
	for k, v := range kvs {
		writeBatch.Put([]byte(k), []byte(v))
	}
<<<<<<< HEAD
	err := kv.db.Write(kv.writeOptions, writeBatch)
=======
	err := kv.DB.Write(kv.WriteOptions, writeBatch)
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	return err
}

func (kv *RocksdbKV) RemoveWithPrefix(prefix string) error {
<<<<<<< HEAD
<<<<<<< HEAD
	kv.readOptions.SetPrefixSameAsStart(true)
	kv.db.Close()
	kv.opts.SetPrefixExtractor(gorocksdb.NewFixedPrefixTransform(len(prefix)))
	var err error
	kv.db, err = gorocksdb.OpenDb(kv.opts, kv.GetName())
=======
=======
	if kv.DB == nil {
		return errors.New("Rocksdb instance is nil when do RemoveWithPrefix")
	}
>>>>>>> knowhere-2
	kv.ReadOptions.SetPrefixSameAsStart(true)
	kv.DB.Close()
	kv.Opts.SetPrefixExtractor(gorocksdb.NewFixedPrefixTransform(len(prefix)))
	var err error
	kv.DB, err = gorocksdb.OpenDb(kv.Opts, kv.GetName())
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	if err != nil {
		return err
	}

<<<<<<< HEAD
	iter := kv.db.NewIterator(kv.readOptions)
=======
	iter := kv.DB.NewIterator(kv.ReadOptions)
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	defer iter.Close()
	iter.Seek([]byte(prefix))
	for ; iter.Valid(); iter.Next() {
		key := iter.Key()
<<<<<<< HEAD
		err := kv.db.Delete(kv.writeOptions, key.Data())
=======
		err := kv.DB.Delete(kv.WriteOptions, key.Data())
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
		if err != nil {
			return nil
		}
	}
	if err := iter.Err(); err != nil {
		return err
	}
	return nil
}

func (kv *RocksdbKV) Remove(key string) error {
<<<<<<< HEAD
<<<<<<< HEAD
	err := kv.db.Delete(kv.writeOptions, []byte(key))
=======
=======
	if kv.DB == nil {
		return errors.New("Rocksdb instance is nil when do Remove")
	}
>>>>>>> knowhere-2
	err := kv.DB.Delete(kv.WriteOptions, []byte(key))
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	return err
}

func (kv *RocksdbKV) MultiRemove(keys []string) error {
	if kv.DB == nil {
		return errors.New("Rocksdb instance is nil when do MultiRemove")
	}
	writeBatch := gorocksdb.NewWriteBatch()
	defer writeBatch.Clear()
	for _, key := range keys {
		writeBatch.Delete([]byte(key))
	}
<<<<<<< HEAD
	err := kv.db.Write(kv.writeOptions, writeBatch)
=======
	err := kv.DB.Write(kv.WriteOptions, writeBatch)
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	return err
}

func (kv *RocksdbKV) MultiSaveAndRemove(saves map[string]string, removals []string) error {
	if kv.DB == nil {
		return errors.New("Rocksdb instance is nil when do MultiSaveAndRemove")
	}
	writeBatch := gorocksdb.NewWriteBatch()
	defer writeBatch.Clear()
	for k, v := range saves {
		writeBatch.Put([]byte(k), []byte(v))
	}
	for _, key := range removals {
		writeBatch.Delete([]byte(key))
	}
<<<<<<< HEAD
	err := kv.db.Write(kv.writeOptions, writeBatch)
=======
	err := kv.DB.Write(kv.WriteOptions, writeBatch)
	return err
}

func (kv *RocksdbKV) DeleteRange(startKey, endKey string) error {
	if kv.DB == nil {
		return errors.New("Rocksdb instance is nil when do DeleteRange")
	}
	writeBatch := gorocksdb.NewWriteBatch()
	defer writeBatch.Clear()
	if len(startKey) == 0 {
		iter := kv.DB.NewIterator(kv.ReadOptions)
		defer iter.Close()
		iter.SeekToFirst()
		startKey = string(iter.Key().Data())
	}

	writeBatch.DeleteRange([]byte(startKey), []byte(endKey))
	err := kv.DB.Write(kv.WriteOptions, writeBatch)
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	return err
}

func (kv *RocksdbKV) MultiRemoveWithPrefix(keys []string) error {
	panic("not implement")
}
<<<<<<< HEAD
=======

>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
func (kv *RocksdbKV) MultiSaveAndRemoveWithPrefix(saves map[string]string, removals []string) error {
	panic("not implement")
}
