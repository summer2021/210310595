// Copyright (C) 2019-2020 Zilliz. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
// with the License. You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software distributed under the License
// is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
// or implied. See the License for the specific language governing permissions and limitations under the License.

package proxy

import (
	"encoding/json"
)

type dummyRequestType struct {
	RequestType string `json:"request_type"`
}

func parseDummyRequestType(str string) (*dummyRequestType, error) {
	drt := &dummyRequestType{}
	if err := json.Unmarshal([]byte(str), &drt); err != nil {
		return nil, err
	}
	return drt, nil
}

<<<<<<< HEAD
type dummyRetrieveRequest struct {
=======
type dummyQueryRequest struct {
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70
	RequestType    string   `json:"request_type"`
	DbName         string   `json:"dbname"`
	CollectionName string   `json:"collection_name"`
	PartitionNames []string `json:"partition_names"`
<<<<<<< HEAD
	Ids            []int64  `json:"ids"`
	OutputFields   []string `json:"output_fields"`
}

func parseDummyRetrieveRequest(str string) (*dummyRetrieveRequest, error) {
	dr := &dummyRetrieveRequest{}
=======
	Expr           string   `json:"expr"`
	OutputFields   []string `json:"output_fields"`
}

func parseDummyQueryRequest(str string) (*dummyQueryRequest, error) {
	dr := &dummyQueryRequest{}
>>>>>>> f00f882d1096abb15825382134050b80d90e4b70

	if err := json.Unmarshal([]byte(str), &dr); err != nil {
		return nil, err
	}
	return dr, nil
}
